﻿/// Copyright 2019 Besting IT, info@besting-it.de
/// Licensed under The MIT License
/// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
/// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using BestingIT.LambdaLink;
using LambdaLinkTest;

namespace LambdaLinkApp
{
    class Program
    {
        public static object GetPropertyValue(object obj, string propertyName)
        {
            return obj.GetType().GetProperty(propertyName).GetValue(obj, null);
        }

        static void Main(string[] args)
        {
            Logger.Log.Level = LogLevel.Info;
            var rnd = new Random();
            var node = new Node(Guid.NewGuid().ToString(), false, 5555 + rnd.Next(1, 1024));
            node.Start();

            TestA a = new TestA();
            a.PropertyChanged += (sender, arg) =>
            {
                Console.WriteLine("Event: value " + arg.PropertyName + " changed to: " + GetPropertyValue(a, arg.PropertyName).ToString());
            };
            node.SynchronizeObject("obj", a);

            Task.Run(() =>
            {
                while (true)
                {
                    Thread.Sleep(3000);
                    //for (int i = 0; i < 10; i++)
                    {
                        a.I = rnd.Next(1, 1024);
                        Console.WriteLine("Set: value changed to: " + a.I);
                    }
                }
            });

            Console.ReadLine();
        }
    }
}
