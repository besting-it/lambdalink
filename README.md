# LambdaLink
LambdaLink is a small, portable .NET Standard library for propagating .NET INotifyPropertyChanged events over network connections. Licensed under The MIT License.

## Quick start
For synchronizing objects using property-changed events, a **Node** object must be created. Each node must have a network-wide, **unique ID**. Node objects discover other node objects and form a **cluster**. Clusters can be seperated by defining **channels**.
Properties of objects can then be easily synchronized by passing them to a node. Property value changes will then be synchronized between equal objects with equal names on equal channels.

Example:
```csharp
// Create a node and register custom objects
var node = new Node("id_123_abc");  // Each node must have a unique ID
var myObj1 = new MyObject1(); // Implements INotifyPropertyChanged
var myObj2 = new MyObject2(); // Implements INotifyPropertyChanged
node.SynchronizeObject("myObject1", myObj1); // Use a unique name for the synchronized object
node.SynchronizeObject("myObject2", myObj2); // Use the same names and types on remote nodes
node.Start();

// Besides local changes, objects will also receive change events of remote objects
myObj1.PropertyChanged += (sender, args) => { /* ... */ }
myObj2.PropertyChanged += (sender, args) => { /* ... */ }

// Start changing properties. Objects registered on other nodes (with the same channel) will receive change events
myObj1.Property1 = 1;
myObj2.Property2 = 2;

// If sync not needed anymore, stop the node
node.Stop();
```
Using the above code block in different places will synchronize MyObject1's and MyObject2's properties bidirectional. *For the synchronization to work, the same unique object **names** and **types** must be used on synchronizing nodes!*

## Channels
Only nodes with the same channel will find each other and synchronize properties. If not defined otherwise, each node will be part of the default channel. To restrict the channel and configure node's visibility, define a custom channels.

Example:
```csharp
var node = new Node("id_123_abc") { Channel = "Custom" };
```

## Cluster requirements
A cluster is defined as **consitent** if all required node IDs have been discovered. The node conataing such requirements will fire an event (see next section) as soon as all required nodes are visible.

Example:
```csharp
var node = new Node("id_123_abc") { Channel = "Custom" };
node1.AddRequiredNodeId("id_234_uvw");
node1.AddRequiredNodeId("id_567_xyz");
```

## Events
There are two event types of each node one can register to

1. ClusterChangedEvent
2. CommunicationErrorEvent

Example:
```csharp
var node = new Node("id_123_abc") { Channel = "Custom" };
node.ClusterChangedEvent += (sender, args) =>
{
	if (args.ClusterState == ClusterConfigurationEventsArgs.State.Consistent)
	{
		// Cluster is consistent, all nodes online
		var ids = args.CurrentClusterNodeIds;
		// ...
	}
	else if (args.ClusterState == ClusterConfigurationEventsArgs.State.Erratic)
	{
		// Cluster is erratic, nodes missing
		var ids = args.CurrentClusterNodeIds;
		// ...
	}	
};
```

## Node modes
There are 3 different modes.

1. Bidirectional (default)
2. Send-only: node only sends out changing properties.
3. Receive-only: node only receives changing properties.
4. Discovery: node only used for the discovery of other modes (see section below)

Example:
```csharp
var node = new Node("id_123_abc") { SyncMode = Node.Mode.SendOnly };
```

## Centralized discovery (no multicast)
The default discovery mode is multicast. When using a centralized discovery mode and setting discovery IP and port on other nodes, unicast discovery will be used instead.

Example:
```csharp
// Creating a discovery server node using port 5555
var discoveryNode = new Node("disco_xyz", false, 5555) { SyncMode = Node.Mode.Discovery };
// Restrict network interface to the one with this IP (by default, all interfaces are used)
discoveryNode.BindLocalIpFilter = "192.168.1.1";
discoveryNode.Start();
```

Example for nodes using centralized discovery node:
```csharp
// Creating a discovery server node using port 5555
var node = new Node("id_123_abc");
node1.DiscoveryIp = "192.168.1.2";
node1.DiscoveryPort = 5555;
```

## Push and pull
Property data will only be transmitted **on change**. Therefore nodes can *push* and *pull* all or certain object properties at any time.

Example:
```csharp
var node = new Node("id_123_abc") { Channel = "Custom" };
node.ClusterChangedEvent += (sender, args) =>
{
	if (args.ClusterState == ClusterConfigurationEventsArgs.State.Consistent)
	{
		// Might be useful: push all properties of all objects when cluster is consistent
		node.PushProperties();
	}
};
```

## Quality of service
LambdaLink uses reliable UDP messaging. For this, QoS parameters can be defined which configure different messaging parameters.

1. Node announce interval in ms
3. Node discovery timeout interval in ms
4. Maximum send message repeat count
5. Maximum message confirmation interval in ms

There are predefined sets for **low**, **high** and **default** QoS-parameters available. The default can be changed using the QoS property of a node.

Example:
```csharp
var node = new Node("id_123_abc");
node.QualityOfService = QoS.High;  // Lower delays, higher resend counts
```

## Network interface binding and IPv6
By default, all available network interfaces are used for communication. Also, ports are assigned automatically starting from a start address (6565).
A node can be restricted to use a specific network interface and port.

Example:
```csharp
var node = new Node("id_123_abc", false, 1234);  // Use local port 1234
node.BindLocalIpFilter = "192.168.1.1"  // Use network interface with specific IP address
```

IPv6 adresses can be used by setting the 2nd costructor parameter to *true*.

## Pushing properties once
Every property change of a registered object will lead to a message send over the network. In case of many properties this will generate much traffic.
Instead of registering objects, a snapshot of the current property values can be sent using the following function. Each function call will generate
a single message. Registering an object on the sender side must be omitted. On the receiving target nodes, objects must be registered as shown before.

Example (send):
```csharp
var node = new Node("id_123_abc");
var myObj1 = new MyObject1();  // Implements INotifyPropertyChanged
//node.SynchronizeObject("myObject1", myObj1);  // Only do this on the remote node, not here!
myObj1.Property1 = 1;
myObj1.Property2 = 2;
myObj1.Property3 = 3;
node.PushOnce("myObject1", myObj1);  // Send all current property values to all cluster nodes
myObj1.Property1 = 4;
myObj1.Property2 = 5;
myObj1.Property3 = 6;
node.PushOnce("myObject1", myObj1);  // Send again
```

## Capabilities and limitations
LambdaLink stores and restores classes with primitive types, decimals, enums, strings, nullables, null values and nested complex types containing the latter.
All references are kept.

However, collection properties are skipped since the property type must be known at compile time. If you want to support collection or 
dictionary properties, you have to introduce a string property that handles the serialization / deserialization. An easy way to do so
is to use a library such as JSON.NET.

## Logging
The log level can be adjusted to different levels.

Example:
```csharp
Logger.Log.Level = LogLevel.Trace;
```

For implementing a custom log output, redefine the default logger output action.

Example:
```csharp
// Set a new log output action and log level
Logger.Log.Output = (str) => Debug.WriteLine(str);
Logger.Log.Level = LogLevel.Info;
```

The default logger logs to the system console.