﻿/// Copyright 2019 Besting IT, info@besting-it.de
/// Licensed under The MIT License
/// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
/// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Threading;
using BestingIT.LambdaLink;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace LambdaLinkTest
{
    public class TestA : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        int i;
        string s;
        TestB b = new TestB();

        public int I { get => i; set { i = value; Notify("I"); } }
        public string S { get => s; set { s = value; Notify("S"); } }
        public TestB B { get => b; set { b = value; Notify("B"); } }

        private void Notify(string name)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }
    }

    public class TestB : INotifyPropertyChanged
    {
        public enum Enm
        {
            Enm1,
            Enm2
        }

        public event PropertyChangedEventHandler PropertyChanged;

        Decimal d;
        Enm e;
        bool? n;

        public Decimal D { get => d; set { d = value; Notify("D"); } }
        public Enm E { get => e; set { e = value; Notify("E"); } }
        public bool? N { get => n; set { n = value; Notify("N"); } }

        private void Notify(string name)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }
    }

    [TestClass]
    public class UnitTest
    {
        [TestCleanup]
        public void testClean()
        {
        }

        [TestInitialize]
        public void testInit()
        {
            Logger.Log.Output = (str) => Debug.WriteLine(str);
            Logger.Log.Level = LogLevel.Trace;
        }

        [TestMethod]
        public void TestSerialization()
        {
            var a = new TestA();
            a.I = 42;
            a.S = "test~~" + Environment.NewLine;
            a.B.D = new decimal(3.14);
            a.B.E = TestB.Enm.Enm2;
            a.B.N = true;

            string str = Serializer.SerializeProperties(a);
            bool intNotified = false;
            bool strNotified = false;
            bool objNotified = false;
            bool dblNotified = false;
            bool enmNotified = false;
            bool nulNotified = false;
            a.PropertyChanged += (sender, args) =>
            {
                if (args.PropertyName == "I")
                    intNotified = true;
                if (args.PropertyName == "S")
                    strNotified = true;
                if (args.PropertyName == "B")
                    objNotified = true;
            };
            a.B.PropertyChanged += (sender, args) =>
            {
                if (args.PropertyName == "D")
                    dblNotified = true;
                if (args.PropertyName == "E")
                    enmNotified = true;
                if (args.PropertyName == "N")
                    nulNotified = true;
            };

            a.I = 0;
            a.S = null;
            a.B.D = 0;
            a.B.E = TestB.Enm.Enm1;
            a.B.N = null;
            Serializer.DeserializeProperties(a, str);

            Assert.IsTrue(intNotified);
            Assert.IsTrue(strNotified);
            // Since we didn't explicitly provide a property name, B is not notified (complex property)
            // Note: B would be notified here, if set to null itself.
            Assert.IsFalse(objNotified);
            Assert.IsTrue(dblNotified);
            Assert.IsTrue(enmNotified);
            Assert.IsTrue(nulNotified);
            Assert.AreEqual(42, a.I);
            Assert.AreEqual("test~~" + Environment.NewLine, a.S);
            Assert.AreEqual(new decimal(3.14), a.B.D);
            Assert.AreEqual(TestB.Enm.Enm2, a.B.E);
            Assert.AreEqual(true, a.B.N);

            str = Serializer.SerializeProperties(a, "B");
            // Setting complex properties to null must work, too
            a.B = null;
            Serializer.DeserializeProperties(a, str, "B");

            // We provided "B" as property name, now we should have been notified
            Assert.IsTrue(objNotified);

            // Check primitive null set
            a.S = null;
            a.B.N = null;
            str = Serializer.SerializeProperties(a);
            a.S = "test";
            a.B.N = false;
            Serializer.DeserializeProperties(a, str);
            Assert.IsNull(a.S);
            Assert.IsNull(a.B.N);
            // Check complex null set
            a.B = null;
            str = Serializer.SerializeProperties(a);
            a.B = new TestB();
            Serializer.DeserializeProperties(a, str);
            Assert.IsNull(a.B);
        }

        [TestMethod]
        public void TestDiscovery()
        {
            var node1 = new Node("id1");
            var node2 = new Node("id2");
            var node3 = new Node("id3");
            bool node1Consistent = false;
            node1.ClusterChangedEvent += (sender, args) =>
            {
                node1Consistent = args.ClusterState == ClusterConfigurationEventsArgs.State.Consistent;
            };
            node1.Start();
            node1.AddRequiredNodeId("id2");
            node1.AddRequiredNodeId("id3");
            node2.Start();
            node3.Start();

            Thread.Sleep(3000);
            Assert.IsTrue(node1Consistent);

            node2.Stop();
            node3.Stop();

            Thread.Sleep(2000);
            Assert.IsFalse(node1Consistent);

            node2.Start();
            node3.Start();
            Thread.Sleep(2000);
            Assert.IsTrue(node1Consistent);
            node2.QualityOfService = new QoS()
            {
                AnnounceIntervalMillis = int.MaxValue,
                TimeoutIntervalMillis = 3000,
                MaxSendRepeatCount = 8
            };
            node3.QualityOfService = new QoS()
            {
                AnnounceIntervalMillis = int.MaxValue,
                TimeoutIntervalMillis = 3000,
                MaxSendRepeatCount = 8
            };

            // Due to timeout
            Thread.Sleep(4000);
            Assert.IsFalse(node1Consistent);

            node1.Stop();
            node2.Stop();
            node3.Stop();
        }

        [TestMethod]
        public void TestSynchronizeIpV4()
        {
            TestSynchronize(false);
        }

        [TestMethod]
        public void TestSynchronizeIpV6()
        {
            TestSynchronize(true);
        }

        public void TestSynchronize(bool ipV6)
        {
            var node1 = new Node("id1", ipV6);
            var node2 = new Node("id2", ipV6);
            var node3 = new Node("id3", ipV6);
            node1.AddRequiredNodeId("id2");
            node1.AddRequiredNodeId("id3");
            var constistent = new AutoResetEvent(false);
            node1.ClusterChangedEvent += (sender, args) =>
            {
                if (args.ClusterState == ClusterConfigurationEventsArgs.State.Consistent)
                    constistent.Set();
            };
            node1.Start();
            node2.Start();
            node3.Start();
            if (!constistent.WaitOne(3000))
            {
                throw new Exception("Failed.");
            }
            var an1 = new TestA();
            node1.SynchronizeObject("obj", an1);
            var an2 = new TestA();
            node2.SynchronizeObject("obj", an2);
            var an3 = new TestA();
            node3.SynchronizeObject("obj", an3);
            an1.I = 42;
            an1.S = "test";
            an1.B = new TestB() { D = new decimal(3.14), E = TestB.Enm.Enm2, N = true };

            Thread.Sleep(1000);
            Assert.AreEqual(an1.I, an2.I);
            Assert.AreEqual(an1.S, an2.S);
            Assert.AreEqual(an1.B.D, an2.B.D);
            Assert.AreEqual(an1.B.E, an2.B.E);
            Assert.AreEqual(an1.B.N, an2.B.N);
            Assert.AreEqual(an1.I, an3.I);
            Assert.AreEqual(an1.S, an3.S);
            Assert.AreEqual(an1.B.D, an3.B.D);
            Assert.AreEqual(an1.B.E, an3.B.E);
            Assert.AreEqual(an1.B.N, an3.B.N);

            // Push & pull
            var an4 = new TestA();
            var node4 = new Node("id4", ipV6);
            node4.SynchronizeObject("obj", an4);
            node4.AddRequiredNodeId("id1");
            var constistentPull = new AutoResetEvent(false);
            node4.ClusterChangedEvent += (sender, args) =>
            {
                if (args.ClusterState == ClusterConfigurationEventsArgs.State.Consistent)
                {
                    // Pull explizit (node 1 should push)
                    node4.PullProperties();
                    constistentPull.Set();
                }
            };
            node4.Start();
            if (!constistentPull.WaitOne(2000))
            {
                throw new Exception("Failed.");
            }
            Thread.Sleep(1000);
            Assert.AreEqual(an1.I, an4.I);
            Assert.AreEqual(an1.S, an4.S);
            Assert.AreEqual(an1.B.D, an4.B.D);
            Assert.AreEqual(an1.B.E, an4.B.E);
            Assert.AreEqual(an1.B.N, an4.B.N);

            // Push once
            node1.ReleaseObject("obj");
            an1.I = 64;
            node1.PushOnce("obj", an1);
            Thread.Sleep(1000);
            Assert.AreEqual(an1.I, an2.I);
            Assert.AreEqual(an1.I, an3.I);
            Assert.AreEqual(an1.I, an4.I);
            an1.I = 128;
            node1.PushOnce("obj", an1, "I");
            Thread.Sleep(1000);
            Assert.AreEqual(an1.I, an2.I);
            Assert.AreEqual(an1.I, an3.I);
            Assert.AreEqual(an1.I, an4.I);

            node1.Stop();
            node2.Stop();
            node3.Stop();
            node4.Stop();
        }

    }
}
