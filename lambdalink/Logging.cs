﻿/// Copyright 2019 Besting IT, info@besting-it.de
/// Licensed under The MIT License
/// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
/// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

using System;
using System.Text;

namespace BestingIT.LambdaLink
{
    public interface ILogger
    {
        LogLevel Level { get; set; }
        Action<string> Output { get; set; }
        void OnLogged(object sender, LogEventArgs e);
    }

    public class DefaultLogger : ILogger
    {
        public LogLevel Level { get; set; } = LogLevel.Off;

        public Action<string> Output { get; set; } = Console.WriteLine;

        public void OnLogged(object sender, LogEventArgs e)
        {
            if (e.LogLevel >= Level)
            {
                var sb = new StringBuilder();
                sb.Append("[");
                if (sender != null)
                {
                    if (sender is string)
                    {
                        sb.Append(sender as string);
                    }
                    else
                    {
                        sb.Append(sender.GetType().Name);
                    }
                    sb.Append(" | ");
                }
                sb.Append(DateTime.Now.ToString());
                sb.Append(" | ");
                sb.Append(e.LogLevel);
                sb.Append("] ");
                sb.Append(e.Message);
                Output(sb.ToString());
            }
        }

    }

    public static class Logger
    {
        public static ILogger Log { get; set; } = new DefaultLogger();
    }

    public enum LogLevel
    {
        Trace = 0,
        Debug = 1,
        Info = 2,
        Warn = 3,
        Error = 4,
        Off = 5
    }

    public class LogEventArgs : EventArgs
    {
        public LogEventArgs(LogLevel level, string message, params string[] values)
        {
            LogLevel = level;
            Message = values != null? string.Format(message, values) : message;
        }

        public string Message { get; set; }
        public LogLevel LogLevel { get; set; }
    }

}
