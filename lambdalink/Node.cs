﻿/// Copyright 2019 Besting IT, info@besting-it.de
/// Licensed under The MIT License
/// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
/// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Net.Sockets;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;

namespace BestingIT.LambdaLink
{

    #region eventargs

    public class ClusterConfigurationEventsArgs : EventArgs
    {
        public enum State
        {
            Consistent,
            Erratic,
            NodeListChanged
        }
        public List<string> CurrentClusterNodeIds { get; set; }
        public State ClusterState { get; set; }
    }

    public class CommunicationErrorEventsArgs
    {
        public enum ErrorCode
        {
            None,  // Ok
            ProtocolVersionMismatch,  // Remote node has wrong protocol version
            MessageFailure  // Max delivery attempts exceeded
        }

        public ErrorCode Code { get; internal set; }

    }

    public class CommunicationMessageErrorEventsArgs<T> : CommunicationErrorEventsArgs where T : Message
    {
        public T FailedMessage { get; internal set; }
    }

    #endregion

    /// <summary>
    /// Instance of this class represent endpoint nodes in a cluster.
    /// Nodes can be described by channels to seperate communication concerns.
    /// </summary>
    public partial class Node : IDisposable
    {

        public const string Version = "1.0";

        public enum Mode
        {
            Bidirectional,
            SendOnly,
            ReceiveOnly,
            Discovery
        }

        const int DefaultPortStart = 6565;
        const int MaxMsgIdCount = 64;

        public event EventHandler<ClusterConfigurationEventsArgs> ClusterChangedEvent;
        public event EventHandler<CommunicationErrorEventsArgs> CommunicationErrorEvent;

        event EventHandler<LogEventArgs> Log = Logger.Log.OnLogged;

        Messenger msg;
        System.Timers.Timer discoveryTimer = new System.Timers.Timer();
        bool consistentNotified = false;
        CancellationTokenSource ctsDelay = new CancellationTokenSource();
        object syncDelay = new object();

        ConcurrentDictionary<string, bool> requiredNodes = new ConcurrentDictionary<string, bool>();
        ConcurrentDictionary<string, bool> serializingIndicator = new ConcurrentDictionary<string, bool>();
        ConcurrentDictionary<string, INotifyPropertyChanged> syncedObjects = new ConcurrentDictionary<string, INotifyPropertyChanged>();
        ConcurrentDictionary<Announce, DateTime> cluster = new ConcurrentDictionary<Announce, DateTime>();

        // Flow control structs
        BlockingCollection<Message> messageQueue = new BlockingCollection<Message>(new ConcurrentQueue<Message>());  // Current send message queue
        CancellationTokenSource cts;
        ConcurrentDictionary<string, DateTime> receivedMsgIds = new ConcurrentDictionary<string, DateTime>();  // Discard doubles
        ConcurrentDictionary<string, bool> confirmedMsgIds = new ConcurrentDictionary<string, bool>();  // Ensure delivery
        private QoS qualityOfService = QoS.Default;

        public Node(string nodeId, bool ipV6 = false, int localServerPort = 0)
        {
            NodeId = nodeId;
            msg = new Messenger(localServerPort == 0 ? PortStart++ : localServerPort, ipV6);
        }

        public static int PortStart { get; set; } = DefaultPortStart;
        public string NodeId { get; private set; }
        public string Channel { get; set; } = "Default";
        public bool Enabled { get; private set; } = false;
        public string BindLocalIpFilter { get => msg.BindLocalIpFilter; set { if (Enabled) throw new InvalidOperationException("Node already started!"); msg.BindLocalIpFilter = value; } }
        public string DiscoveryIp { get => msg.DiscoveryIp; set { if (Enabled) throw new InvalidOperationException("Node already started!"); msg.DiscoveryIp = value; } }
        public int DiscoveryPort { get => msg.DiscoveryPort; set { if (Enabled) throw new InvalidOperationException("Node already started!"); msg.DiscoveryPort = value; } }
        public Mode SyncMode { get; set; } = Mode.Bidirectional;
        public int LocalPort { get => msg.LocalPort; }

        /// <summary>
        /// Start the node.
        /// </summary>
        public void Start()
        {
            if (Enabled)
                throw new InvalidOperationException("Node already started!");
            msg.Open();
            InitUnicastReceiver();
            InitMulticastReceiver();
            messageQueue = new BlockingCollection<Message>(new ConcurrentQueue<Message>());
            cts = new CancellationTokenSource();
            StartQueueProcessing(cts.Token);
            SendAnnounce();
            discoveryTimer = new System.Timers.Timer();
            discoveryTimer.Elapsed += OnDiscoveryTimerElapsed;
            discoveryTimer.Disposed += OnDiscoveryTimerDisposed;
            discoveryTimer.Interval = QualityOfService.AnnounceIntervalMillis;
            discoveryTimer.Enabled = true;
            Log(this, new LogEventArgs(LogLevel.Info, "Node {0} started, addrs={1}, port={2}.", NodeId, msg.LocalAddrString, msg.LocalPort.ToString()));
            Enabled = true;
        }

        /// <summary>
        /// Stop the node.
        /// </summary>
        public void Stop()
        {
            if (!Enabled)
                throw new InvalidOperationException("Node not started!");
            discoveryTimer.Enabled = false;
            discoveryTimer.Dispose();
            cts.Cancel();
            messageQueue.Dispose();
            msg.Close();
            receivedMsgIds.Clear();
            confirmedMsgIds.Clear();
            Log(this, new LogEventArgs(LogLevel.Info, "Node {0} stopped.", NodeId));
            Enabled = false;
        }

        /// <summary>
        /// Send current object properties once to the cluster without registering the object permanently.
        /// The object must not be registered on this node.
        /// </summary>
        /// <param name="name">A unique name of the object</param>
        /// <param name="obj">The object holding properties</param>
        /// <param name="propertyName">The property name, or null for all properties</param>
        /// <exception cref="InvalidOperationException">If node is in wrong mode or object is registered permanently</exception>
        public void PushOnce(string name, INotifyPropertyChanged obj, string propertyName = null)
        {
            if (SyncMode == Mode.ReceiveOnly)
                throw new InvalidOperationException("Node is in receive-only mode!");
            if (SyncMode == Mode.Discovery)
                throw new InvalidOperationException("Node is in discovery mode!");
            if (syncedObjects.ContainsKey(name))
                throw new InvalidOperationException("Object is registered for synchronizing on this node!");
            if (cluster.Count == 0)
            {
                Log(this, new LogEventArgs(LogLevel.Trace, "Node {0} push once transmit skipped due to empty cluster", NodeId));
                return;
            }
            Log(this, new LogEventArgs(LogLevel.Trace, "Node {0} transmitting push once, name: {1}, property: {2}", NodeId, name, propertyName));
            string serialized = Serializer.SerializeProperties(obj, propertyName);
            SendSynchronize(name, serialized, null, propertyName);
        }

        /// <summary>
        /// Register object for synchronizing property changes with other node(s) in the cluster optionally defined by channel.
        /// </summary>
        /// <param name="name">A unique name of the object</param>
        /// <param name="obj">The object holding properties</param>
        /// <exception cref="InvalidOperationException">If the object is already registered, null or node is in wrong mode</exception>
        public void SynchronizeObject(string name, INotifyPropertyChanged obj)
        {
            if (!syncedObjects.TryAdd(name, obj))
                throw new InvalidOperationException("Object already registered!");
            if (name == null || obj == null)
                throw new NullReferenceException("Null objects not allowed!");
            if (SyncMode == Mode.Discovery)
                throw new InvalidOperationException("Node is in discovery mode!");
            obj.PropertyChanged += (sender, args) =>
            {
                // If properties change due to restore action, we do not handle the event to prevent bouncing
                if (!serializingIndicator.ContainsKey(name))
                {
                    if (cluster.Count == 0)
                    {
                        Log(this, new LogEventArgs(LogLevel.Trace, "Node {0} sync transmit skipped due to empty cluster", NodeId));
                        return;
                    }
                    Log(this, new LogEventArgs(LogLevel.Trace, "Node {0} transmitting sync, name: {1}, type: {2}, property: {3}", NodeId, name, sender.GetType().Name, args.PropertyName));
                    string serialized = Serializer.SerializeProperties(obj, args.PropertyName);
                    SendSynchronize(name, serialized, null, args.PropertyName);
                }
            };
        }

        /// <summary>
        /// Stop synchronizing property changes in the cluster
        /// </summary>
        /// <param name="name">The unique object name</param>
        /// <exception cref="KeyNotFoundException">If key is not found</exception>
        public void ReleaseObject(string name)
        {
            if (!syncedObjects.ContainsKey(name))
                throw new KeyNotFoundException("Key not found!");
            syncedObjects.TryRemove(name, out _);
        }

        public void Dispose()
        {
            Stop();
        }

        /// <summary>
        /// Add required node id. Node becomes consistent if all required nodes are up.
        /// </summary>
        /// <param name="id"></param>
        public void AddRequiredNodeId(string id)
        {
            requiredNodes.TryAdd(id, true);
        }

        /// <summary>
        /// Remove required node id.
        /// </summary>
        /// <param name="id"></param>
        public void RemoveRequiredNodeId(string id)
        {
            requiredNodes.TryRemove(id, out _);
        }

        /// <summary>
        /// Push all a properties to remote nodes.
        /// </summary>
        /// <param name="objName">The object's name, or null for all objects</param>
        /// <exception cref="InvalidOperationException">If calling this method in wrong mode</exception>
        public void PushProperties(string objName = null)
        {
            if (SyncMode == Mode.ReceiveOnly)
                throw new InvalidOperationException("Node is in receive-only mode!");
            if (SyncMode == Mode.Discovery)
                throw new InvalidOperationException("Node is in discovery mode!");
            foreach (var next in syncedObjects)
            {
                if (objName == null || objName == next.Key)
                {
                    // Serialize all properties of the object
                    string serialized = Serializer.SerializeProperties(next.Value);
                    // Send to all nodes
                    SendSynchronize(next.Key, serialized);
                }
            }
        }

        /// <summary>
        /// Pull properties from remote nodes.
        /// </summary>
        /// <param name="objName">The object's name, or null for all objects</param>
        /// <exception cref="InvalidOperationException">If calling this method in wrong mode</exception>
        public void PullProperties(string objName = null)
        {
            if (SyncMode == Mode.SendOnly)
                throw new InvalidOperationException("Node is in send-only mode!");
            if (SyncMode == Mode.Discovery)
                throw new InvalidOperationException("Node is in discovery mode!");
            SendPull(objName);
        }

        #region private

        public QoS QualityOfService
        {
            private get => qualityOfService;
            set
            {
                if (Enabled)
                {
                    DiscoveryAnnounceInterval = value.AnnounceIntervalMillis;
                }
                qualityOfService = value;
            }
        }

        double DiscoveryAnnounceInterval
        {
            set
            {
                if (!Enabled)
                {
                    throw new InvalidOperationException("Node not started!");
                }
                discoveryTimer.Interval = value;
            }
        }

        void StartQueueProcessing(CancellationToken token)
        {
            Task.Run(async () =>
            {
                Message nextMsg = null;
                try
                {
                    while (true)
                    {
                        token.ThrowIfCancellationRequested();
                        if (nextMsg == null)
                            nextMsg = messageQueue.Take();
                        if (ShouldSend(nextMsg))
                        {
                            await Send(nextMsg);
                            if ((nextMsg is Announce) || (nextMsg is Discontinue))
                            {
                                nextMsg = null;
                            }
                            else
                            {
                                try
                                {
                                    await Task.Delay(qualityOfService.MaxMessageConfirmDelay, AquireToken());
                                }
                                catch { }
                                if (confirmedMsgIds.ContainsKey(nextMsg.MsgId))
                                {
                                    confirmedMsgIds.TryRemove(nextMsg.MsgId, out _);
                                    nextMsg = null;
                                }
                            }
                        }
                        else
                        {
                            nextMsg = null;
                        }
                    }
                }
                catch (Exception e) when (e is TaskCanceledException || e is ObjectDisposedException)
                {
                }
            });
        }

        CancellationToken AquireToken()
        {
            lock (syncDelay)
            {
                ctsDelay = new CancellationTokenSource();
                return ctsDelay.Token;
            }
        }

        private async Task<bool> Send(Message nextMsg)
        {
            bool sent = true;
            try
            {
                await msg.SendAsync(nextMsg.TargetAddrs, nextMsg.TargetPort, Frames.ObjectToString(nextMsg));
            }
            catch (Exception ex) when (ex is InvalidCastException | ex is SocketException)
            {
                Log(this, new LogEventArgs(LogLevel.Error, ex.Message));
                sent = false;
            }
            catch (Exception ex) when (ex is ObjectDisposedException)
            {
                sent = false;
            }
            return sent;
        }

        bool ShouldSend(Message msg)
        {
            if (!(msg is Announce) && !(msg is Discontinue))
            {
                if (msg.SendCounter <= QualityOfService.MaxSendRepeatCount)
                {
                    msg.SendCounter++;
                    return true;
                }
                else
                {
                    // Handle repeatedly occured send failure attempts
                    Log(this, new LogEventArgs(LogLevel.Debug, "Node {0} reports max send attempts exceeded while sending to node {1}", msg.SourceNodeId, msg.TargetNodeId));
                    if (msg is Synchronize)
                    {
                        CommunicationErrorEvent?.Invoke(this, new CommunicationMessageErrorEventsArgs<Synchronize>() { Code = CommunicationErrorEventsArgs.ErrorCode.MessageFailure, FailedMessage = msg as Synchronize });
                    }
                    else if (msg is Pull)
                    {
                        CommunicationErrorEvent?.Invoke(this, new CommunicationMessageErrorEventsArgs<Pull>() { Code = CommunicationErrorEventsArgs.ErrorCode.MessageFailure, FailedMessage = msg as Pull });
                    }
                    return false;
                }
            }
            return true;
        }

        void OnDiscoveryTimerElapsed(object sender, ElapsedEventArgs e)
        {
            SendAnnounce();
            CheckTimeouts();
            CheckRequirements();
            ClearOldestMsgIdHalf();
        }

        void ClearOldestMsgIdHalf()
        {
            if (receivedMsgIds.Count > MaxMsgIdCount)
            {
                var enttries = receivedMsgIds.AsEnumerable().OrderBy(p => p.Value.Ticks);
                var en = enttries.GetEnumerator();
                int count = 0;
                while (en.MoveNext())
                {
                    receivedMsgIds.TryRemove(en.Current.Key, out _);
                    count++;
                    if (count >= receivedMsgIds.Count / 2)
                        break;
                }
            }
        }

        void CheckTimeouts()
        {
            var toRemove = new HashSet<Announce>();
            foreach (var entry in cluster)
            {
                var span = DateTime.Now - entry.Value;
                if (span.TotalMilliseconds > QualityOfService.TimeoutIntervalMillis)
                {
                    toRemove.Add(entry.Key);
                }
            }
            foreach (var key in toRemove)
            {
                Log(this, new LogEventArgs(LogLevel.Info, "Node {0} timed out.", key.SourceNodeId));
                cluster.TryRemove(key, out _);
            }
            if (toRemove.Count > 0)
                LogCluster();
        }

        void CheckRequirements()
        {
            if (requiredNodes.Count == 0)
            {
                return;
            }
            foreach (var nextId in requiredNodes.Keys)
            {
                if (!cluster.ContainsKey(new Announce() { SourceNodeId = nextId }))
                {
                    // Requirements not okay
                    if (consistentNotified)
                    {
                        Log(this, new LogEventArgs(LogLevel.Info, "Node {0} is erratic.", NodeId));
                        ClusterChangedEvent?.Invoke(this, new ClusterConfigurationEventsArgs()
                        {
                            CurrentClusterNodeIds = cluster.Keys.Select(announced => announced.SourceNodeId).ToList(),
                            ClusterState = ClusterConfigurationEventsArgs.State.Erratic
                        });
                        consistentNotified = false;
                    }
                    return;
                }
            }
            // Requirements okay
            if (!consistentNotified)
            {
                Log(this, new LogEventArgs(LogLevel.Info, "Node {0} is consistent.", NodeId));
                ClusterChangedEvent?.Invoke(this, new ClusterConfigurationEventsArgs()
                {
                    CurrentClusterNodeIds = cluster.Keys.Select(announced => announced.SourceNodeId).ToList(),
                    ClusterState = ClusterConfigurationEventsArgs.State.Consistent
                });
                consistentNotified = true;
            }
        }

        async void OnDiscoveryTimerDisposed(object sender, EventArgs e)
        {
            await SendDiscontinue();
        }

        void SendMessage(Message msg)
        {
            messageQueue.Add(msg);
        }

        #endregion
    }
}
