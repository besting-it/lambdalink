﻿/// Copyright 2019 Besting IT, info@besting-it.de
/// Licensed under The MIT License
/// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
/// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization.Json;
using System.Text;

namespace BestingIT.LambdaLink
{
    public static class Serializer
    {
        static event EventHandler<LogEventArgs> Log = Logger.Log.OnLogged;

        public static string ObjectToString<T>(T obj)
        {
            using (var stream = new MemoryStream())
            {
                var ser = new DataContractJsonSerializer(typeof(T));
                ser.WriteObject(stream, obj);
                return Encoding.ASCII.GetString(stream.ToArray());
            }
        }

        public static T StringToObject<T>(string str)
        {
            using (var stream = new MemoryStream(Encoding.ASCII.GetBytes(str)))
            {
                var ser = new DataContractJsonSerializer(typeof(T));
                T obj = (T)ser.ReadObject(stream);
                return obj;
            }
        }

        public static string SerializeProperties(object baseObj, string propertyName = null)
        {
            return SerializeProperties(baseObj, propertyName, new Dictionary<string, string>(), 0);
        }

        public static string SerializeProperties(object baseObj, string propertyName, Dictionary<string, string> props, int level)
        {
            string objectName = baseObj.GetType().Name;
            PropertyInfo[] pInfo = baseObj.GetType().GetProperties();
            foreach (PropertyInfo pi in pInfo)
            {
                if ((propertyName == null || propertyName == pi.Name) && pi.GetGetMethod() != null && pi.GetSetMethod() != null)
                {
                    string key = pi.Name + "$" + level;
                    // Duplicates possible due to hidden base properties
                    if (!props.ContainsKey(key))
                    {
                        try
                        {
                            object objVal = pi.GetValue(baseObj);
                            SerializePropertyValue(props, level, key, objVal);
                        }
                        catch (Exception e)
                        {
                            // Skip all that throw exceptions, but log to debug
                            Log("Serializer", new LogEventArgs(LogLevel.Debug, e.Message));
                        }
                    }
                }
            }
            if (level == 0)
            {
                return EncodeBase64(string.Join(Environment.NewLine, props.Select(d => d.Key + "~" + d.Value)));
                //return string.Join(Environment.NewLine, props.Select(d => d.Key + "~" + d.Value));
            }
            return null;
        }

        private static void SerializePropertyValue(Dictionary<string, string> props, int level, string key, object objVal)
        {
            if (objVal == null)
            {
                props.Add(key, "null");
                return;
            }
            Type t = objVal.GetType();
            if (t.IsPrimitive || t.IsEnum || t == typeof(decimal) || t == typeof(string) || t == typeof(Nullable))
            {
                // Add simple properties
                string value = objVal.ToString();
                if (t == typeof(string))
                {
                    // Possible escape chars in strings
                    value = EncodeBase64(value);
                }
                props.Add(key, value);
            }
            else if (t != typeof(IEnumerable))
            {
                // Recursive descent into complex properties
                SerializeProperties(objVal, null, props, level + 1);
            }
        }

        public static void DeserializeProperties(object baseObj, string properties, string propertyName = null)
        {
            var props = new Dictionary<string, string>();
            using (var strReader = new StringReader(DecodeBase64(properties)))
            //using (var strReader = new StringReader(properties))
            {
                string next;
                while ((next = strReader.ReadLine()) != null)
                {
                    var keyValueStr = next.Split('~');
                    props.Add(keyValueStr[0], keyValueStr[1]);
                }
            }
            DeserializeProperties(baseObj, props, propertyName, 0);
        }

        private static void DeserializeProperties(object baseObj, Dictionary<string, string> props, string propertyName, int level)
        {
            string objectName = baseObj.GetType().Name;
            PropertyInfo[] pInfo = baseObj.GetType().GetProperties();
            foreach (PropertyInfo pi in pInfo)
            {
                if ((propertyName == null || propertyName == pi.Name) && pi.GetGetMethod() != null && pi.GetSetMethod() != null)
                {
                    if (pi.CanWrite)
                    {
                        try
                        {
                            DeserializePropertyValue(baseObj, props, level, pi);
                            if (propertyName != null && baseObj is INotifyPropertyChanged)
                            {
                                // Raise property changed by setting the same value again (complex types only in case of previous recursive descent)
                                Type t = pi.PropertyType;
                                if (!t.IsPrimitive && !t.IsEnum && t != typeof(decimal) && t != typeof(string) && t != typeof(Nullable))
                                    pi.SetValue(baseObj, pi.GetValue(baseObj));
                            }
                        }
                        catch (Exception e)
                        {
                            // Skip all that throw exceptions, but log to debug
                            Log("Deserializer", new LogEventArgs(LogLevel.Debug, e.Message));
                        }
                    }
                }
            }
        }

        private static void DeserializePropertyValue(object baseObj, Dictionary<string, string> props, int level, PropertyInfo pi)
        {
            string key = pi.Name + "$" + level;
            if (!props.ContainsKey(key) && pi.PropertyType != typeof(IEnumerable))
            {
                // Different level, complex property
                object nextBaseObj = pi.GetValue(baseObj);
                if (nextBaseObj == null)
                {
                    // If the object is null, we need to create a new instance
                    nextBaseObj = Activator.CreateInstance(pi.PropertyType);
                    DeserializeProperties(nextBaseObj, props, null, level + 1);
                    pi.SetValue(baseObj, nextBaseObj);
                    return;
                }
                DeserializeProperties(nextBaseObj, props, null, level + 1);
                return;
            }
            string strValue = props[key];
            if (strValue == "null")
            {
                // New value set explicitly to null
                pi.SetValue(baseObj, null);
                return;
            }
            Type baseType = pi.PropertyType.BaseType;
            if (baseType == typeof(Enum))
            {
                // Set enums
                var enumType = Type.GetType(pi.PropertyType.AssemblyQualifiedName);
                var objVal = Enum.Parse(enumType, strValue);
                pi.SetValue(baseObj, objVal);                
            }
            else if (pi.PropertyType.IsGenericType && pi.PropertyType.GetGenericTypeDefinition().Equals(typeof(Nullable<>)))
            {
                // Set nullable primitives
                Type subType = Nullable.GetUnderlyingType(pi.PropertyType);
                var objVal = Convert.ChangeType(strValue, subType);
                pi.SetValue(baseObj, objVal);
            }
            else
            {
                var objVal = Convert.ChangeType(strValue, pi.PropertyType);
                Type t = objVal.GetType();
                if (t.IsPrimitive || t == typeof(decimal) || t == typeof(string))
                {
                    // Set simple properties
                    if (t == typeof(string))
                    {
                        // Possible escape chars in strings
                        objVal = DecodeBase64(objVal as string);
                    }
                    pi.SetValue(baseObj, objVal);
                }
            }
        }

        public static string EncodeBase64(string source)
        {
            var bytes = Encoding.UTF8.GetBytes(source);
            return Convert.ToBase64String(bytes);
        }

        public static string DecodeBase64(string source)
        {
            var bytes = Convert.FromBase64String(source);
            return Encoding.UTF8.GetString(bytes);
        }

    }

}
