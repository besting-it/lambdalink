﻿/// Copyright 2019 Besting IT, info@besting-it.de
/// Licensed under The MIT License
/// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
/// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

using System;
using System.Collections.Generic;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;

namespace BestingIT.LambdaLink
{
    class IPv6Joiner : IMulticastJoiner
    {
        event EventHandler<LogEventArgs> Log = Logger.Log.OnLogged;

        public List<MulticastServer> CreateMulticastServers(AddressFamily addrFamily, IPAddress mcastAdr, int mcastPort, string ipFilter)
        {
            var serverList = new List<MulticastServer>();
            NetworkInterface[] networkInterfaces = NetworkInterface.GetAllNetworkInterfaces();
            foreach (NetworkInterface networkInterface in networkInterfaces)
            {
                if (IsInvalidNIC(networkInterface))
                    continue;
                var ipv6InterfaceProperties = networkInterface.GetIPProperties().GetIPv6Properties();
                foreach (var uca in networkInterface.GetIPProperties().UnicastAddresses)
                {
                    if (uca.Address == null)
                        continue;
                    if (uca.Address.AddressFamily != AddressFamily.InterNetworkV6)
                        continue;
                    try
                    {
                        var ip = uca.Address.ToString();
                        if (ipFilter == null || ip.ToUpperInvariant().StartsWith(ipFilter.ToUpperInvariant()))
                        {
                            if (!Utils.IsMulticast(mcastAdr.ToString()))
                            {
                                serverList.Add(new MulticastServer() { Server = null, LocalEndpointAdr = ip });
                                break;
                            }
                            var ipv6MulticastOption = new IPv6MulticastOption(mcastAdr);
                            ipv6MulticastOption.InterfaceIndex = networkInterface.GetIPProperties().GetIPv6Properties().Index;
                            var udpMulticastServer = new UdpClient(addrFamily);
                            udpMulticastServer.Client.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.ReuseAddress, true);
                            udpMulticastServer.Client.Bind(new IPEndPoint(IPAddress.Parse(ip), mcastPort));
                            udpMulticastServer.JoinMulticastGroup((int)ipv6MulticastOption.InterfaceIndex, ipv6MulticastOption.Group);
                            serverList.Add(new MulticastServer() { Server = udpMulticastServer, LocalEndpointAdr = ip });
                            Log(this, new LogEventArgs(LogLevel.Info, "UDP multicast joining: {0} on {1}", mcastAdr.ToString(), ip));
                            break;
                        }
                    }
                    catch (SocketException)
                    {
                        Log(this, new LogEventArgs(LogLevel.Error, "UDP multicast joining error: {0}", mcastAdr.ToString()));
                        continue;
                    }
                }
            }

            return serverList;
        }

        bool IsInvalidNIC(NetworkInterface networkInterface)
        {
            return (!networkInterface.Supports(NetworkInterfaceComponent.IPv6)
                || (networkInterface.OperationalStatus != OperationalStatus.Up)
                || (networkInterface.GetIPProperties().GetIPv6Properties().Index == NetworkInterface.LoopbackInterfaceIndex));
        }

    }
}
