﻿/// Copyright 2019 Besting IT, info@besting-it.de
/// Licensed under The MIT License
/// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
/// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

using System;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.Text;

namespace BestingIT.LambdaLink
{

    public static class Frames
    {
        static event EventHandler<LogEventArgs> Log = Logger.Log.OnLogged;
        public const string ProtocolVersion = "1.0";

        public static string ObjectToString<T>(T obj) where T : Message
        {
            using (var stream = new MemoryStream())
            {
                var ser = new DataContractJsonSerializer(typeof(Message));  // Restrict to base to incluse type in JSON
                ser.WriteObject(stream, obj);
                return Encoding.ASCII.GetString(stream.ToArray());
            }
        }

        public static T StringToObject<T>(string str) where T : Message
        {
            using (var stream = new MemoryStream(Encoding.ASCII.GetBytes(str)))
            {
                var ser = new DataContractJsonSerializer(typeof(T));
                T obj = (T)ser.ReadObject(stream);
                return obj;
            }
        }

    }

    [DataContract]
    [KnownType(typeof(MessageResponse))]
    [KnownType(typeof(Announce))]
    [KnownType(typeof(Discontinue))]
    [KnownType(typeof(Synchronize))]
    [KnownType(typeof(Pull))]
    public class Message
    {
        [DataMember]
        public string ProtocolVersion { get; set; } = Frames.ProtocolVersion;
        [DataMember]
        public string SourceAddrs { get; set; }
        [DataMember]
        public int SourcePort { get; set; }
        [DataMember]
        public string MsgId { get; set; }
        [DataMember]
        public string SourceNodeId { get; set; }
        [DataMember]
        public string TargetNodeId { get; set; }
        [DataMember]
        public string TargetAddrs { get; set; }
        [DataMember]
        public int TargetPort { get; set; }
        public int SendCounter { get; set; }
    }

    [DataContract]
    class MessageResponse : Message
    {
        [DataMember]
        public string RefMsgId { get; set; }
    }

    [DataContract]
    class Announce : Message
    {
        [DataMember]
        public string Channel { get; set; }

        public override bool Equals(object obj)
        {
            return (obj as Message).SourceNodeId == SourceNodeId;
        }

        public override int GetHashCode()
        {
            return SourceNodeId.GetHashCode();
        }
    }

    [DataContract]
    class Discontinue : Message
    {
    }

    [DataContract]
    public class Synchronize : Message
    {
        [DataMember]
        public string ObjectName { get; set; }
        [DataMember]
        public string PropertyName { get; set; }
        [DataMember]
        public string PropertyData { get; set; }
    }

    [DataContract]
    public class Pull : Message
    {
        [DataMember]
        public string ObjectName { get; set; }
    }

}
