﻿/// Copyright 2019 Besting IT, info@besting-it.de
/// Licensed under The MIT License
/// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
/// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BestingIT.LambdaLink
{
    class Utils
    {
        const int MinMatchLength = 4;

        /// <summary>
        /// Get the best list item that matches longest prefix of given string.
        /// </summary>
        /// <param name="str">The given string</param>
        /// <param name="list">The string list</param>
        /// <returns>The best matching list item, or null</returns>
        internal static string GetBestPrefixMatch(string str, ICollection<string> list)
        {
            var matches = new Dictionary<string, int>();
            foreach (var next in list)
                matches.Add(next, 0);
            // Start comparing prefix lengths
            int longestMax = 0;
            foreach (var next in list)
            {
                for (int i = 0; i < next.Length; i++)
                {
                    if (i < str.Length)
                    {
                        int current = matches[next];
                        if (next[i] == str[i])
                        {
                            current++;
                            longestMax = Math.Max(current, longestMax);
                            matches[next] = current;
                        }
                        else
                        {
                            if (current < longestMax)
                            {
                                // Better ones available
                                matches.Remove(next);
                                break;
                            }
                        }
                    }
                }
                // Already found best
                if (matches.Count == 1)
                {
                    if (longestMax >= MinMatchLength)
                        return matches.First().Key;
                    else
                        return null;
                }
            }
            if (longestMax < MinMatchLength)
                return null;
            // Sort by match length and get last value (longest match)
            var sorted = matches.AsEnumerable().OrderBy(e => e.Value).ToList();
            return sorted.Last().Key;
        }

        internal static bool IsMulticast(string ip)
        {
            if (ip.Contains('.'))
            {
                // ipv4
                return int.Parse(ip.Substring(0, ip.IndexOf('.'))) >= 224;
            }
            else if (ip.Contains(':'))
            {
                // ipv6
                return ip.ToUpperInvariant().StartsWith("FF0");
            }
            return false;
        }

    }
}
