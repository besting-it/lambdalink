﻿/// Copyright 2019 Besting IT, info@besting-it.de
/// Licensed under The MIT License
/// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
/// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

using System;
using System.Collections.Generic;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;

namespace BestingIT.LambdaLink
{
    class IPv4Joiner : IMulticastJoiner
    {
        event EventHandler<LogEventArgs> Log = Logger.Log.OnLogged;

        public List<MulticastServer> CreateMulticastServers(AddressFamily addrFamily, IPAddress mcastAdr, int mcastPort, string ipFilter)
        {
            var serverList = new List<MulticastServer>();
            var epGroup = new IPEndPoint(mcastAdr, mcastPort);
            NetworkInterface[] networkInterfaces = NetworkInterface.GetAllNetworkInterfaces();
            foreach (NetworkInterface networkInterface in networkInterfaces)
            {
                if (IsInvalidNIC(networkInterface))
                    continue;
                foreach (var uac in networkInterface.GetIPProperties().UnicastAddresses)
                {
                    if (uac.Address == null)
                        continue;
                    if (uac.Address.AddressFamily != AddressFamily.InterNetwork)
                        continue;
                    try
                    {
                        var ip = uac.Address.ToString();
                        if (ipFilter == null || ip.ToUpperInvariant().StartsWith(ipFilter.ToUpperInvariant()))
                        {
                            if (!Utils.IsMulticast(mcastAdr.ToString()))
                            {
                                serverList.Add(new MulticastServer() { Server = null, LocalEndpointAdr = ip });
                                break;
                            }
                            var udpMulticastServer = new UdpClient(addrFamily);
                            udpMulticastServer.Client.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.ReuseAddress, true);
                            udpMulticastServer.Client.Bind(new IPEndPoint(IPAddress.Parse(ip), mcastPort));
                            udpMulticastServer.JoinMulticastGroup(epGroup.Address, uac.Address);
                            serverList.Add(new MulticastServer() { Server = udpMulticastServer, LocalEndpointAdr = ip});
                            Log(this, new LogEventArgs(LogLevel.Info, "UDP multicast joining: {0} on {1}", epGroup.Address.ToString(), ip));
                            break;
                        }
                    }
                    catch (SocketException)
                    {
                        Log(this, new LogEventArgs(LogLevel.Error, "UDP multicast joining error: {0} on {1}", epGroup.Address.ToString(), uac.Address.ToString()));
                        continue;
                    }
                }
            }

            return serverList;
        }

        bool IsInvalidNIC(NetworkInterface networkInterface)
        {
            return (!networkInterface.Supports(NetworkInterfaceComponent.IPv4)
                || (networkInterface.OperationalStatus != OperationalStatus.Up)
                || (networkInterface.GetIPProperties().GetIPv4Properties().Index == NetworkInterface.LoopbackInterfaceIndex));
        }
    }
}
