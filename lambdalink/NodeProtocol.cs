﻿/// Copyright 2019 Besting IT, info@besting-it.de
/// Licensed under The MIT License
/// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
/// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

using System;
using System.ComponentModel;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace BestingIT.LambdaLink
{
    public partial class Node
    {
        object syncMCast = new object();
        object syncUCast = new object();

        static object syncLog = new object();

        void SendPull(string objectName = null)
        {
            foreach (var nextNode in cluster.Keys)
            {
                Pull message = new Pull()
                {
                    ObjectName = objectName,
                };
                PreSendInitMessage(message, nextNode);
                Log(this, new LogEventArgs(LogLevel.Trace, "Node {0} transmitting pull to {1}:{2}, msg ID: {3}", NodeId, message.TargetAddrs, message.TargetPort.ToString(), message.MsgId));
                SendMessage(message);
            }
        }

        void SendSynchronize(string objectName, string propertyData, string targetNodeId = null, string propertyName = null)
        {
            if (SyncMode == Mode.ReceiveOnly)
            {
                Log(this, new LogEventArgs(LogLevel.Trace, "Node in receive-only mode. Sync skipped."));
                return;
            }
            foreach (var nextNode in cluster.Keys)
            {
                if (targetNodeId != null && targetNodeId != nextNode.SourceNodeId)
                    continue;
                Synchronize message = new Synchronize()
                {
                    ObjectName = objectName,
                    PropertyData = propertyData,
                    PropertyName = propertyName,
                };
                PreSendInitMessage(message, nextNode);
                Log(this, new LogEventArgs(LogLevel.Trace, "Node {0} transmitting sync to {1}:{2}, msg ID: {3}", NodeId, message.TargetAddrs, message.TargetPort.ToString(), message.MsgId));
                SendMessage(message);
            }
        }

        void SendAnnounce()
        {
            if (SyncMode == Mode.Discovery)
                return;
            //Log(this, new LogEventArgs(LogLevel.Trace, "Node {0} sends hello.", NodeId));
            Announce message = new Announce()
            {
                TargetAddrs = msg.DiscoveryIp,
                TargetPort = msg.DiscoveryPort,
                Channel = Channel
            };
            PreSendInitMessage(message);
            SendMessage(message);
        }

        async Task SendDiscontinue()
        {
            if (SyncMode == Mode.Discovery)
                return;
            //Log(this, new LogEventArgs(LogLevel.Trace, "Node {0} sends bye.", NodeId));
            Discontinue message = new Discontinue()
            {
                TargetAddrs = msg.DiscoveryIp,
                TargetPort = msg.DiscoveryPort,
            };
            PreSendInitMessage(message);
            await Send(message);
        }

        void PreSendInitMessage(Message message, Announce targetNode = null)
        {
            message.MsgId = Guid.NewGuid().ToString();
            message.SourceNodeId = NodeId;
            message.SourceAddrs = msg.LocalAddrString;
            message.SourcePort = msg.LocalPort;
            if (targetNode != null)
            {
                message.TargetNodeId = targetNode.SourceNodeId;
                message.TargetAddrs = targetNode.SourceAddrs;
                message.TargetPort = targetNode.SourcePort;
            }
        }

        void InitMulticastReceiver()
        {
            msg.MulticastReceivedEvent += (sender, args) =>
            {
                lock (syncMCast)
                {
                    if (args.Incoming.Contains(typeof(Announce).Name))
                    {
                        ReceiveAnnounce(args);
                    }
                    else if (args.Incoming.Contains(typeof(Discontinue).Name))
                    {
                        ReceiveDiscontinue(args);
                    }
                }
            };
        }

        void ReceiveDiscontinue(MessageReceivedEventArgs args)
        {
            var bye = Frames.StringToObject<Discontinue>(args.Incoming);
            //Log(this, new LogEventArgs(LogLevel.Trace, "Node {0} receives bye from node {1}.", NodeId, bye.SourceNodeId));
            if (bye.SourceNodeId != NodeId && cluster.ContainsKey(new Announce() { SourceNodeId = bye.SourceNodeId }))
            {
                Log(this, new LogEventArgs(LogLevel.Info, "Node {0} received bye from: {1}", NodeId, bye.SourceNodeId));
                cluster.TryRemove(new Announce() { SourceNodeId = bye.SourceNodeId }, out _);
                ClusterChangedEvent?.Invoke(this, new ClusterConfigurationEventsArgs()
                {
                    CurrentClusterNodeIds = cluster.Keys.Select(announced => announced.SourceNodeId).ToList(),
                    ClusterState = ClusterConfigurationEventsArgs.State.NodeListChanged
                });
                CheckRequirements();
                LogCluster();
            }
            if (SyncMode == Mode.Discovery)
                Relay(bye);
        }

        void ReceiveAnnounce(MessageReceivedEventArgs args)
        {
            var hello = Frames.StringToObject<Announce>(args.Incoming);
            //Log(this, new LogEventArgs(LogLevel.Trace, "Node {0} receives hello from node {1}.", NodeId, hello.SourceNodeId));
            if (hello.SourceNodeId != NodeId && (hello.Channel == Channel || SyncMode == Mode.Discovery))
            {
                bool newMember = !cluster.ContainsKey(hello);
                if (newMember)
                {
                    Log(this, new LogEventArgs(LogLevel.Info, "Node {0} received hello from: {1}, channel: {2}", NodeId, hello.SourceNodeId, hello.Channel));
                    ClusterChangedEvent?.Invoke(this, new ClusterConfigurationEventsArgs()
                    {
                        CurrentClusterNodeIds = cluster.Keys.Select(announced => announced.SourceNodeId).ToList(),
                        ClusterState = ClusterConfigurationEventsArgs.State.NodeListChanged
                    });
                }
                cluster.AddOrUpdate(hello, DateTime.Now, (key, oldValue) => DateTime.Now);
                if (newMember)
                {
                    CheckRequirements();
                    LogCluster();
                }
            }
            if (SyncMode == Mode.Discovery)
                Relay(hello);
        }

        private void LogCluster()
        {
            lock(syncLog)
            {
                Log(this, new LogEventArgs(LogLevel.Info, "Node cluster is ["));
                foreach (var next in cluster.Keys)
                {
                    Log(this, new LogEventArgs(LogLevel.Info, "\tRemote node {0}, addrs={1}, port={2}", next.SourceNodeId, next.SourceAddrs, next.SourcePort.ToString()));
                }
                Log(this, new LogEventArgs(LogLevel.Info, "\tThis node {0}, addrs={1}, port={2}", NodeId, msg.LocalAddrString, msg.LocalPort.ToString()));
                Log(this, new LogEventArgs(LogLevel.Info, "] {0}, LLv{1}", SyncMode.ToString(), Version));
            }
        }

        private void Relay(Message msg)
        {
            foreach (var nextNode in cluster.Keys)
            {
                if (msg.SourceNodeId != nextNode.SourceNodeId)
                {
                    msg.TargetAddrs = nextNode.SourceAddrs;
                    msg.TargetPort = nextNode.SourcePort;
                    msg.TargetNodeId = nextNode.SourceNodeId;
                    Log(this, new LogEventArgs(LogLevel.Trace, "Node {0} transmitting relay msg to {1}:{2}, msg ID: {3}", NodeId, msg.TargetAddrs, msg.TargetPort.ToString(), msg.MsgId));
                    SendMessage(msg);
                }
            }
        }

        void InitUnicastReceiver()
        {
            msg.UnicastReceivedEvent += (sender, args) =>
            {
                lock(syncUCast)
                {
                    Message incoming = Frames.StringToObject<Message>(args.Incoming);
                    if (incoming.ProtocolVersion != Frames.ProtocolVersion)
                    {
                        Log(this, new LogEventArgs(LogLevel.Error, "Node {0} received wrong protocol version from node {1}", NodeId, incoming.SourceNodeId));
                        CommunicationErrorEvent?.Invoke(this, new CommunicationErrorEventsArgs() { Code = CommunicationErrorEventsArgs.ErrorCode.ProtocolVersionMismatch });
                        return;
                    }

                    // Evaluate message received confirmation
                    if (args.Incoming.Contains(typeof(MessageResponse).Name))
                    {
                        var incomingResponse = Frames.StringToObject<MessageResponse>(args.Incoming);
                        if (confirmedMsgIds.TryAdd(incomingResponse.RefMsgId, true))
                        {
                            Log(this, new LogEventArgs(LogLevel.Trace, "Node {0} received confirmation of msg ID: {1}", NodeId, incomingResponse.RefMsgId));
                            lock (syncDelay)
                            {
                                ctsDelay.Cancel();
                            }
                        }
                        // No confirmation of confirmation messages                    
                        return;
                    }

                    if (!receivedMsgIds.TryAdd(incoming.MsgId, DateTime.Now))
                        // We already got that one, just confirm it again
                        goto confirm;

                    HandleIncomingMessages(args);

                // Always generate received confirmation in unicast case
                confirm:
                    // Unicast discovery messages only occur in centralized discovery mode
                    if (args.Incoming.Contains(typeof(Announce).Name) || args.Incoming.Contains(typeof(Discontinue).Name))
                        return;
                    var messageResponse = new MessageResponse()
                    {
                        RefMsgId = incoming.MsgId,
                        TargetAddrs = incoming.SourceAddrs,
                        TargetPort = incoming.SourcePort
                    };
                    PreSendInitMessage(messageResponse);
                    args.Outgoing = new OutgoingMessage()
                    {
                        Addrs = messageResponse.TargetAddrs,
                        Port = messageResponse.TargetPort,
                        Content = Frames.ObjectToString(messageResponse)
                    };
                    Log(this, new LogEventArgs(LogLevel.Trace, "Node {0} sends confirmation of msg ID: {1}", NodeId, messageResponse.RefMsgId));
                }
            };
        }

        void HandleIncomingMessages(MessageReceivedEventArgs args)
        {
            if (args.Incoming.Contains(typeof(Synchronize).Name))
            {
                ReceiveSynchronize(args);
            }
            else if (args.Incoming.Contains(typeof(Pull).Name))
            {
                ReceivePull(args);
            }
            // For non-multicast discovery
            else if (args.Incoming.Contains(typeof(Announce).Name))
            {
                ReceiveAnnounce(args);
            }
            else if (args.Incoming.Contains(typeof(Discontinue).Name))
            {
                ReceiveDiscontinue(args);
            }
        }

        void ReceiveSynchronize(MessageReceivedEventArgs args)
        {
            if (SyncMode == Mode.SendOnly)
            {
                Log(this, new LogEventArgs(LogLevel.Trace, "Node {0} in send-only mode. Sync skipped."));
                return;
            }
            var sync = Frames.StringToObject<Synchronize>(args.Incoming);
            Log(this, new LogEventArgs(LogLevel.Trace, "Node {0} receiving sync, name: {1}, property: {2}", NodeId, sync.ObjectName, sync.PropertyName));
            syncedObjects.TryGetValue(sync.ObjectName, out INotifyPropertyChanged obj);
            if (obj == null)
            {
                Log(this, new LogEventArgs(LogLevel.Debug, "Object skipped since not mapped on this node, name = {0}, node {1}", sync.ObjectName, NodeId));
                return;
            }
            // Store object name to prevent bouncing sync message due to property changed event during deserialization
            serializingIndicator.TryAdd(sync.ObjectName, true);
            Serializer.DeserializeProperties(obj, sync.PropertyData, sync.PropertyName);
            serializingIndicator.TryRemove(sync.ObjectName, out _);
        }

        void ReceivePull(MessageReceivedEventArgs args)
        {
            var pull = Frames.StringToObject<Pull>(args.Incoming);
            string objName = pull.ObjectName;
            Log(this, new LogEventArgs(LogLevel.Trace, "Node {0} receiving pull, name: {1}", NodeId, objName));
            foreach (var next in syncedObjects)
            {
                if (objName == null || objName == next.Key)
                {
                    // Serialize all properties of the object
                    string serialized = Serializer.SerializeProperties(next.Value);
                    // Send to specific node id (source node which originated the pull request)
                    SendSynchronize(next.Key, serialized, pull.SourceNodeId);
                }
            }
        }

    }
}
