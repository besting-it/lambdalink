﻿/// Copyright 2019 Besting IT, info@besting-it.de
/// Licensed under The MIT License
/// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
/// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

using System;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Threading;
using System.Collections.Concurrent;

namespace BestingIT.LambdaLink
{

    class UdpState
    {
        public IPEndPoint LocalEndpoint { get; set; }
        public IPEndPoint RemoteEndpoint { get; set; }
        public UdpClient Client { get; set; }
        public byte[] ReceivedData { get; set; }
        public bool IsUnicast { get; set; }
    }

    class OutgoingMessage
    {
        public string Content { get; set; }
        public string Addrs { get; set; }
        public int Port { get; set; }
    }

    class MessageReceivedEventArgs : EventArgs
    {
        public string RemoteEndpointAddr { get; set; }
        public string Incoming { get; set; }
        public OutgoingMessage Outgoing { get; set; }
    }

    class Messenger : IDisposable
    {
        const int MCastDiscoveryPort = 9595;
        const string MCastIpV4 = "224.2.4.4";
        const string MCastIpV6 = "FF02::3";

        public event EventHandler<MessageReceivedEventArgs> UnicastReceivedEvent;
        public event EventHandler<MessageReceivedEventArgs> MulticastReceivedEvent;

        public int LocalPort { get; }
        public HashSet<string> LocalAddrs { get; } = new HashSet<string>();
        public string LocalAddrString { get => string.Join(",", LocalAddrs); }
        public string BindLocalIpFilter { get; set; }
        public string DiscoveryIp { get => discoveryIp; set => discoveryIp = value; }
        public int DiscoveryPort { get => discoveryPort; set => discoveryPort = value; }

        event EventHandler<LogEventArgs> Log = Logger.Log.OnLogged;

        UdpClient udpServer;
        ConcurrentDictionary<string, UdpClient> udpClients = new ConcurrentDictionary<string, UdpClient>();
        List<MulticastServer> udpMulticastServers;

        UdpState unicastState;
        CancellationTokenSource ctsReceive = new CancellationTokenSource();
        IMulticastJoiner joiner;
        string discoveryIp;
        int discoveryPort;
        readonly IPAddress anyAddr;
        readonly AddressFamily addrFamily;
        readonly bool useIpV6 = false;

        public Messenger(int localPort, bool useIpV6 = false)
        {
            this.useIpV6 = useIpV6;
            LocalPort = localPort;
            anyAddr = useIpV6 ? IPAddress.IPv6Any : IPAddress.Any;
            addrFamily = useIpV6 ? AddressFamily.InterNetworkV6 : AddressFamily.InterNetwork;
            discoveryIp = useIpV6 ? MCastIpV6 : MCastIpV4;
            discoveryPort = MCastDiscoveryPort;
            if (useIpV6)
            {
                joiner = new IPv6Joiner();
            }
            else
            {
                joiner = new IPv4Joiner();
            }
        }

        public async Task SendAsync(string ip, int port, string data)
        {
            CheckNull(ip, port, data);
            var dataBytes = Encoding.ASCII.GetBytes(data);
            var targets = ip.Split(',');
            foreach (var nextIp in targets)
            {
                var ep = new IPEndPoint(IPAddress.Parse(nextIp), port);
                if (Utils.IsMulticast(nextIp))
                {
                    // Multicast, use all interfaces
                    foreach (var nextClient in udpClients)
                        await nextClient.Value.SendAsync(dataBytes, dataBytes.Length, ep);
                }
                else
                {
                    // Unicast, use interface matching best with target address
                    string matchingIpKey = Utils.GetBestPrefixMatch(nextIp, udpClients.Keys);
                    if (matchingIpKey == null)
                        continue;
                    if (!udpClients.ContainsKey(matchingIpKey))
                    {
                        Log(this, new LogEventArgs(LogLevel.Error, "Target address not reachable by used network interfaces!"));
                        continue;
                    }
                    var matchingClient = udpClients[matchingIpKey];
                    await matchingClient.SendAsync(dataBytes, dataBytes.Length, ep);
                }
            }
        }

        private static void CheckNull(string ip, int port, string data)
        {
            if (ip == null)
                throw new NullReferenceException("IP cannot be null!");
            if (data == null)
                throw new NullReferenceException("Data cannot be null!");
            if (port == 0)
                throw new NullReferenceException("Port cannot be 0!");
        }

        public void Open()
        {
            ctsReceive.Dispose();
            ctsReceive = new CancellationTokenSource();
            StartServer();
            foreach (string nextLocal in LocalAddrs)
            {
                var unicastEndpoint = new IPEndPoint(IPAddress.Parse(nextLocal), 0);
                var client = new UdpClient(unicastEndpoint);
                udpClients.TryAdd(nextLocal, client);
            }
        }

        void StartServer()
        {
            InitUnicastServer();
            InitMulticastServer();
        }

        public void Dispose()
        {
            Close();
        }

        public void Close()
        {
            ctsReceive.Cancel();
            udpServer.Close();
            foreach (var nextServer in udpMulticastServers)
                nextServer.Server.Close();
            udpMulticastServers.Clear();
            foreach (var nextClient in udpClients)
                nextClient.Value.Close();
            udpClients.Clear();
            LocalAddrs.Clear();
        }

        void InitUnicastServer()
        {
            var unicastEndpoint = new IPEndPoint(anyAddr, LocalPort);
            udpServer = new UdpClient(unicastEndpoint);
            unicastState = new UdpState
            {
                LocalEndpoint = unicastEndpoint,
                Client = udpServer,
                IsUnicast = true
            };
            StartReceiving(udpServer, unicastState);
            Log(this, new LogEventArgs(LogLevel.Info, "UDP unicast endpoint opened: {0}", unicastEndpoint.ToString()));
        }

        void InitMulticastServer()
        {
            if (!Utils.IsMulticast(discoveryIp))
            {
                Log(this, new LogEventArgs(LogLevel.Info, "Multicast deactivated, IP is unicast address: {0}", discoveryIp.ToString()));
            }
            IPAddress mcastAdr = IPAddress.Parse(discoveryIp);
            udpMulticastServers = joiner.CreateMulticastServers(addrFamily, mcastAdr, discoveryPort, BindLocalIpFilter);
            foreach (var nextServer in udpMulticastServers)
            {
                LocalAddrs.Add(nextServer.LocalEndpointAdr);
                var endpoint = new IPEndPoint(IPAddress.Parse(nextServer.LocalEndpointAdr), MCastDiscoveryPort);
                var multicastState = new UdpState
                {
                    LocalEndpoint = endpoint,
                    Client = nextServer.Server,
                    IsUnicast = false
                };
                if (nextServer.Server != null)
                    StartReceiving(nextServer.Server, multicastState);
            }
        }

        private async void StartReceiving(UdpClient server, UdpState state)
        {
            while (true)
            {
                try
                {
                    ctsReceive.Token.ThrowIfCancellationRequested();
                    var res = await server.ReceiveAsync();
                    state.RemoteEndpoint = res.RemoteEndPoint;
                    state.ReceivedData = res.Buffer;
                    await ReceiveCallback(state);
                }
                catch (Exception ex) when (ex is OperationCanceledException || ex is ObjectDisposedException)
                {
                    break;
                }
                catch (SocketException)
                {
                    Log(this, new LogEventArgs(LogLevel.Error, "Error Receiving UDP message."));
                    break;
                }
            }
        }

        async Task ReceiveCallback(UdpState state)
        {
            var args = new MessageReceivedEventArgs()
            {
                Incoming = Encoding.ASCII.GetString(state.ReceivedData),
                RemoteEndpointAddr = state.RemoteEndpoint.Address.ToString()
            };
            if (state.IsUnicast)
            {
                UnicastReceivedEvent?.Invoke(this, args);
                // Check for direct response
                if (args.Outgoing != null)
                {
                    await SendAsync(args.Outgoing.Addrs, args.Outgoing.Port, args.Outgoing.Content);
                }
            }
            else
            {
                MulticastReceivedEvent?.Invoke(this, args);
            }
        }

    }
}
